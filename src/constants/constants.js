//Interface Constants
export const gitRepositoriesHeaderHeight = 150;
export const gitContributorImageWidth = 40;
export const gitContributorImageHeight = 40;

//Apollo Constants
export const gitApiUrl = 'https://api.github.com/graphql';
export const gitToken = '';